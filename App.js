import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
//var myDeck;
//var myImage;
//var datas;
//const counter2 = 26;

export default function App() {
    const [counter, setCounter] = React.useState(0);
    //var myDeck;
    //var myImage;
    //const datas = 0;

    React.useEffect(function () {
        fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
            .then(function (response) {
                return response.json()
            })
            .then(function (data) {
                myDeck = data.deck_id;
                console.log(myDeck)
                datas = data;
            })
        //const response = await fetch ('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1');
        //const data = await response.json();
        //console.log(data);
    }, [])

    /*async function req() {
        const response = await fetch ('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1');
        return await response.json();
    }*/

    return (
        <View style={styles.container}>
            <Button title="Start New Game" onPress={() => setCounter(26)} />
            <Button title="Draw a card" onPress={() => {this.drawMe()}} />
            <Text>You have {counter} cards left before battle</Text>
            <Image source={{ uri: 'myImage' }}
                style={{ width: 100, height: 140 }} />
            <StatusBar style="auto" />
        </View>
    );
}

drawMe = () => {

    React.useEffect(function () {
        fetch('https://deckofcardsapi.com/api/deck/myDeck/draw/?count=1')
            .then(function (response) {
                return response.json()
            })
            .then(function (data) {
                //console.log(data.image)
                //myImage = data.image;
                //display image
            })
    }, [])
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
